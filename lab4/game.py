# ukljucivanje biblioteke pygame
import pygame

pygame.init()
pygame.font.init()

myfont = pygame.font.SysFont('Arial',40)

# definiranje konstanti za velicinu prozora
WIDTH = 1024
HEIGHT = 600
# tuple velicine prozora
size = (WIDTH, HEIGHT)
PURPLE = (255, 0, 255)
YELLOW = (255, 255, 0)
RED = (255, 0, 0)

welcome_text = myfont.render('Zdravo!', True, RED)
                             
#ucitavanje asseta
bgimg = pygame.image.load("assets/bg.jpg")
bgimg = pygame.transform.scale(bgimg, size)

icon = pygame.image.load("assets/icon.png")
icon = pygame.transform.scale(icon,(100,100))
print(bgimg)
print(icon)


#definiranje novog ekrana za igru
screen = pygame.display.set_mode(size)
#definiranje naziva prozora
pygame.display.set_caption("Nasa kul igra")

clock = pygame.time.Clock()

done = False
bgcolour = PURPLE
showbg = True
x = WIDTH/2-50
y = HEIGHT/2-50
diry = 1
dirx = 1
vel = 6
vel2 = vel
pauza = True
while not done:
    #event petlja
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            done = True
        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_ESCAPE:
                pauza = not pauza
                if pauza:
                    vel2 = vel
                    vel = 0
                elif pauza == False:
                    vel = vel2
            if event.key == pygame.K_UP:
                vel += 1
            if event.key == pygame.K_DOWN:
                vel -= 1
            if event.key == pygame.K_q:
                done = True
            if event.key == pygame.K_b:
                showbg = not showbg
            if event.key == pygame.K_SPACE:
                if bgcolour == PURPLE:
                    bgcolour = YELLOW
                elif bgcolour == YELLOW:
                    bgcolour = RED
                else:
                    bgcolour = PURPLE
    #obrada trenutnog stanja framea
    if x >= WIDTH-100 or x <= 0:
        dirx = dirx * -1
    if y >= HEIGHT-100 or y <= 0:
        diry = diry * -1
        
    print (f"pauza: {pauza}")
    
    x += dirx * vel
    y += diry * vel

    
    #iscrtavanja
    vel_text = myfont.render(f"Velocity: {vel}", True, RED)
    screen.fill(bgcolour)
    if showbg:
        screen.blit(bgimg, (0,0))
    screen.blit(icon, (x,y))
    screen.blit(welcome_text, (10,10))
    screen.blit(vel_text, (10,HEIGHT -50))
    
    pygame.display.flip()
    #ukoliko je potrebno ceka do iscrtavanja 
    #iduceg framea kako bi imao 60fpsa
    clock.tick(60)

pygame.quit()


